var http =require('http');
var express = require('express');
var path = require('path');
var _ = require("underscore");

var app = express();
var server = app.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function (){
	console.log("server started")
});
app.use(express.static(__dirname + '/client'));

var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

var urlLists = [];


app.get('/api/ipAddress',function(req,res){
	res.send(req.ip);
});

app.post('/api/getUrlLists',function(req,res){
	var url =req.body.url;
	console.log("recieved message:"+ url);
	var crawlerHelper = require("./crawlerHelper");
	crawlerHelper.c.queue(url);

	urlLists.push(_.uniq(crawlerHelper.arrayUnique));
	res.send(urlLists);
});