var client = angular.module('client', ['ngRoute' ]);


client.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'index.html',
        controller: 'clientController'
      })
      .
      otherwise({
        redirectTo: '/'
      });
  }]);



client.controller('clientController', function($scope, $http, $q) {
	 $http.get("/api/ipAddress").success(function (data){
            console.log(data);
            $scope.ipAddress = data;
    });
    $scope.send = function send() {
          console.log('Sending url:', $scope.url);
          $http.post("/api/getUrlLists",{url:$scope.url}).success(function (data){
            console.log(data);
            $scope.urls = data;
          });
        };
});