moment.locale("en");
moment.fn.fromNowOrNow = function(a) {
    var m = moment(new Date(a));
    if (Math.abs(m.diff(this)) < 60000) {
    	var seconds = parseInt(Math.abs(m.diff(this)) / 1000);
    	if (seconds < 10){
    		return 'few seconds ago';
    	}
        return seconds + ' seconds ago';
    }
    return m.fromNow();
}

client.directive('ago', function($interval) {

    return {
        scope: {
            time: '=time'
        },
        link: function(scope, element) {
            scope.$watch("time", function (){
                if (!scope.time){
                    element.text("loading");
                } else {
                    element.text(moment().fromNowOrNow(scope.time));
                }    
            })
            $interval(function() {
                element.text(moment().fromNowOrNow(scope.time))
            }, 10000)
        }
    };
});